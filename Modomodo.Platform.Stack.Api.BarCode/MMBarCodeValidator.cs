﻿using Modomodo.Platform.Stack.Interfaces;
using ServiceStack.FluentValidation;
using ServiceStack.ServiceInterface;

namespace Modomodo.Platform.Stack.Api.BarCode
{
    public class MMBarCodeValidator : AbstractValidator<MMBarCodeRequest>, IMMPluginValidator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MMBarCodeValidator"/> class.
        /// Validation involves Content (not null and not empty), Width (greater than zero), Height (greater than zero).
        /// </summary>
        public MMBarCodeValidator()
        {
            RuleSet(ApplyTo.Get, () =>
            {
                RuleFor(x => x.Content)
                    .NotNull()
                    .WithMessage("Content can not be null")
                    .NotEmpty()
                    .WithMessage("Content can not be empty");
                RuleFor(x => x.Width).GreaterThan(0).WithMessage("Width must be greater than zero");
                RuleFor(x => x.Height).GreaterThan(0).WithMessage("Height must be greater than zero");
            });
        }
    }
}