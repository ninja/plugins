﻿using System;
using System.Drawing;
using System.Net;
using Modomodo.Platform.Core.Net;
using Modomodo.Platform.Stack.Api.BarCode.Interfaces;
using Modomodo.Platform.Stack.Common;
using Modomodo.Platform.Stack.Interfaces;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Modomodo.Platform.Stack.Api.BarCode
{   
    /// <summary>
    /// This service permits to work with bar codes.
    /// </summary>    
    public class MMBarCode : Service
    {
        /// <summary>
        /// There was en error during bar code generation.
        /// </summary>
        public const string MessageBarCode = "Modomodo.Stack: Error during bar code generation";

        /// <summary>
        /// Accept the GET verb for the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The HttpResult of the perfomed operation.</returns>
        /// <exception cref="StackHttpError">Internal server error.</exception>
        public HttpResult Get(MMBarCodeRequest request)
        {
            try
            {
                var drawBarCode = new DrawBarcode(request.Content, request.BarcodeType, request.Height, request.Width)
                {
                    BackgroundBarCode = request.BackgroundBarCode,
                    ForegroundBarCode = request.ForegroundBarCode,
                    ShowLabelBarCode = request.ShowLabelBarCode,
                    FontSize = request.FontSize,
                    ForceCode128B = request.ForceCode128B
                };
                var barCodeArray = drawBarCode.GetStreamBarCode();
                return new HttpResult(barCodeArray, MediaTypes.Image.Jpeg);
            }
            catch (Exception exception)
            {
                throw new StackHttpError(HttpStatusCode.InternalServerError, exception.Message, MessageBarCode);
            }
        }
    }

    /// <summary>
    /// The Bar Code processing request
    /// </summary>
    [Route("/api/barcode/code/{Content}/w/{Width}/h/{Height}/barcodetype/{BarcodeType}", Verbs = Verbs.Get)]
    public class MMBarCodeRequest : IReturn<HttpResult>
    {
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        [AvoidAttribute]
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        [AvoidAttribute]
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        [AvoidAttribute]
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show the label.
        /// </summary>
        public bool ShowLabelBarCode { get; set; }

        /// <summary>
        /// Gets or sets the bar code background color.
        /// </summary>
        public Color? BackgroundBarCode { get; set; }

        /// <summary>
        /// Gets or sets the bar code foreground color.
        /// </summary>
        public Color? ForegroundBarCode { get; set; }

        /// <summary>
        /// Gets or sets the font size
        /// </summary>
        public int? FontSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to force to code 128B.
        /// </summary>
        public bool ForceCode128B { get; set; }

        /// <summary>
        /// Gets or sets the type of the barcode.
        /// </summary>
        [AvoidAttribute]
        public BarcodeType BarcodeType { get; set; }
    }
}