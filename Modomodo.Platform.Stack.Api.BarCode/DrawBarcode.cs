﻿using System;
using System.Drawing;
using Modomodo.Platform.Stack.Api.BarCode.Interfaces;
using ZXing;

namespace Modomodo.Platform.Stack.Api.BarCode
{
    /// <summary>
    /// This class provides functionalities to convert a barcode number to a bitmap object or a byte array
    /// </summary>
    public class DrawBarcode
    {
        /// <summary>
        /// Gets or sets the barcode number
        /// </summary>
        /// <value>
        /// The barcode number to process (e.g. 3033710074365 in case of EAN-13)
        /// </value>
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the height of the barcode image
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// Gets or sets the width of the barcode image
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to show a label for the barcode
        /// </summary>
        public bool ShowLabelBarCode { get; set; }

        /// <summary>
        /// Gets or sets the background barcode.
        /// </summary>
        /// <value>
        ///  A System.Drawing.Color to set the background color
        /// </value>
        public Color? BackgroundBarCode { get; set; }

        /// <summary>
        /// Gets or sets the foreground bar code.
        /// </summary>
        /// <value>
        ///A System.Drawing.Color to set the foreground color
        /// </value>
        public Color? ForegroundBarCode { get; set; }

        /// <summary>
        /// Gets or sets the size of the font.
        /// </summary>
        public int? FontSize { get; set; }

        /// <summary>
        /// Gets or sets the barcode format
        /// </summary>
        /// <value>
        /// The type of the barcode (e.g. BarcodeType.EAN_13 )
        /// </value>
        public BarcodeType BarcodeType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to force Code128B
        /// </summary>
        public bool ForceCode128B { get; set; }

        private readonly Color _defaultBackground = Color.White;
        private readonly Color _defaultForeground = Color.Black;
        private const int DefaultFontSize = 10;
        private const string DefaultFontName = "Verdana";

        /// <summary>
        /// Initializes a new instance of the <see cref="DrawBarcode" /> class.
        /// </summary>
        /// <param name="content">The barcode number.</param>
        /// <param name="barcodeType">The barcode format.</param>
        /// <param name="height">The height of the barcode image</param>
        /// <param name="width">The width of the barcode image</param>
        public DrawBarcode(string content, BarcodeType barcodeType, int height, int width)
        {
            Content = content;
            BarcodeType = barcodeType;
            Height = height;
            Width = width;
        }

        /// <summary>
        /// Generate a barcode image from the given barcode number
        /// </summary>
        /// <returns>
        /// A Bitmap object representing the barcode
        /// </returns>
        public Bitmap GetImageBarCode()
        {
            BarcodeFormat barcodeFormat;
            if (BackgroundBarCode == null)
                BackgroundBarCode = _defaultBackground;
            if (ForegroundBarCode == null)
                ForegroundBarCode = _defaultForeground;
            if (FontSize == null)
                FontSize = DefaultFontSize;

            Enum.TryParse(BarcodeType.ToString(), true, out barcodeFormat);

            var writer = new BarcodeWriter
            {
                Format = barcodeFormat,
                Options = { Height = Height, Width = Width, PureBarcode = !ShowLabelBarCode },

                Renderer = new ZXing.Rendering.BitmapRenderer
                {
                    Background = BackgroundBarCode.Value,
                    Foreground = ForegroundBarCode.Value,
                    TextFont = new Font(DefaultFontName, FontSize.Value, GraphicsUnit.Pixel)
                }
            };
            if (ForceCode128B)
                writer.Options.Hints[EncodeHintType.CODE128_FORCE_CODESET_B] = true;
            return writer.Write(Content);
        }


        /// <summary>
        /// Generate a stream from the given barcode number
        /// </summary>
        /// <returns>
        /// A byte array object representing the barcode image
        /// </returns>
        public byte[] GetStreamBarCode()
        {
            var bitmap = GetImageBarCode();
            var converter = new ImageConverter();
            return (byte[])converter.ConvertTo(bitmap, typeof(byte[]));
        }
    }
}
