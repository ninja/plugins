﻿namespace Modomodo.Platform.Stack.Api.BarCode.Interfaces
{
    /// <summary>
    /// This enumeration enlist different types of barcode 
    /// formats that can be managed by the DrawBarcode class
    /// </summary>
    public enum BarcodeType
    {
        /// <summary>
        /// Set the AZTEC format
        /// </summary>
        AZTEC = 1,
        /// <summary>
        /// Set the CODABAR format
        /// </summary>
        CODABAR = 2,
        /// <summary>
        /// Set the COD e_39 format
        /// </summary>
        CODE_39 = 4,
        /// <summary>
        /// Set the COD e_93 format
        /// </summary>
        CODE_93 = 8,
        /// <summary>
        /// Set the COD e_128 format
        /// </summary>
        CODE_128 = 16,
        /// <summary>
        /// Set the DAT a_ MATRIX format
        /// </summary>
        DATA_MATRIX = 32,
        /// <summary>
        /// Set the EA N_8 format
        /// </summary>
        EAN_8 = 64,
        /// <summary>
        /// Set the EA N_13 format
        /// </summary>
        EAN_13 = 128,
        /// <summary>
        /// Set the ITF format
        /// </summary>
        ITF = 256,
        /// <summary>
        /// Set the MAXICODE format
        /// </summary>
        MAXICODE = 512,
        /// <summary>
        /// Set the PD F_417 format
        /// </summary>
        PDF_417 = 1024,
        /// <summary>
        /// Set the Q r_ CODE format
        /// </summary>
        QR_CODE = 2048,
        /// <summary>
        /// Set the RS S_14 format
        /// </summary>
        RSS_14 = 4096,
        /// <summary>
        /// Set the RS s_ EXPANDED format
        /// </summary>
        RSS_EXPANDED = 8192,
        /// <summary>
        /// Set the UP c_ A format
        /// </summary>
        UPC_A = 16384,
        /// <summary>
        /// Set the UP c_ E format
        /// </summary>
        UPC_E = 32768,
        /// <summary>
        /// Set the UP c_ EA n_ EXTENSION format
        /// </summary>
        UPC_EAN_EXTENSION = 65536,
        /// <summary>
        /// Set the MSI format
        /// </summary>
        MSI = 131072,
        /// <summary>
        /// Set the All_1 D format
        /// </summary>
        All_1D = 192990,
        /// <summary>
        /// Set the PLESSEY format
        /// </summary>
        PLESSEY = 262144,
    }
}
