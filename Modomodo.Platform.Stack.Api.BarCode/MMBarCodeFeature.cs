﻿using ServiceStack.WebHost.Endpoints;

namespace Modomodo.Platform.Stack.Api.BarCode
{
    public class MMBarCodeFeature : IPlugin
    {
        public void Register(IAppHost appHost)
        {
            appHost.RegisterService(typeof(MMBarCode), new[] { "/api/barcode/code/{Content}/w/{Width}/h/{Height}/barcodetype/{BarcodeType}" });
        }
    }
}