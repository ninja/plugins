﻿using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;
using Modomodo.Platform.Stack.Api.ImageResizer.Utility;
using Modomodo.Platform.Storage.Provider.Amazon;
using Modomodo.Platform.Storage.Provider.Amazon.Configuration;
using Modomodo.Platform.Storage.Provider.Amazon.Models;
using ServiceStack.Common.Web;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Providers
{
    /// <summary>
    /// Amazon provider
    /// </summary>
    public class AmazonProvider : IProviderBase
    {
        public void Save(MMImageResizeRequest imageResizeRequest, byte[] imageResize)
        {
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(new AmazonSettings().GetSection()))
            {
                var objectRequest = new WriteObjectRequest(GetFileName(imageResizeRequest), imageResizeRequest.FileExtension, imageResize);
                objectRequest.SetAcl(Acl.PublicRead);
                amazonClient.Put(objectRequest);
            }
        }

        public HttpResult Get(MMImageResizeRequest imageResizeRequest)
        {
            var fileName = GetFileName(imageResizeRequest);
            var contentType = Helpers.GetContentTypeHttpResult(imageResizeRequest);
            var objectRequest = new ObjectRequest(fileName, imageResizeRequest.FileExtension);
            using (var amazonClient = AmazonClientFactory.CreateAmazonClient(new AmazonSettings().GetSection()))
            {
                var byteImage = amazonClient.Get(objectRequest);
                if (byteImage != null)
                    return new HttpResult(byteImage, contentType);   
            }
            var byteRequest = Helpers.GetByteArrayImage(imageResizeRequest);
            Save(imageResizeRequest, byteRequest);
            return new HttpResult( byteRequest, contentType);
        }

        private static string GetFileName(MMImageResizeRequest imageResizeRequest)
        {
            return imageResizeRequest.FileNameWithExtension.Replace(imageResizeRequest.FileExtension, 
                string.Empty);
        }
    }
}