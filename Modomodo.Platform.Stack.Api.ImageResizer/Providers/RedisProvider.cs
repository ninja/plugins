﻿using System;
using Modomodo.Platform.Core.ExtensionMethods;
using Modomodo.Platform.SERedis.Interfaces;
using Modomodo.Platform.Stack.Api.ImageResizer.Exceptions;
using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;
using Modomodo.Platform.Stack.Api.ImageResizer.Utility;
using Modomodo.Platform.Stack.Common.Configuration;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using ServiceStack.Text;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Providers
{
    /// <summary>
    /// Redis provider
    /// </summary>
    public class RedisProvider : Service, IProviderRedis
    {
        private TimeSpan? _timeoutKey;

        public ISERedisManager SERedisManager
        {
            get
            {
                return base.TryResolve<ISERedisManager>();
            }
        }

        /// <summary>
        /// Timeout key in redis (default 2 day)
        /// </summary>
        public TimeSpan TimeoutKey
        {
            get
            {
                if (_timeoutKey == null)
                    _timeoutKey = new TimeSpan(2, 0, 0, 0);
                return _timeoutKey.Value;
            }
            set
            {
                _timeoutKey = value;
            }
        }

        public void Save(MMImageResizeRequest imageResizeRequest, byte[] imageResize)
        {
            var key = GetKeyImageResizeInRedis(imageResizeRequest);
            SERedisManager.StringSetKeyAsync(key, Convert.ToBase64String(imageResize), GetTimeout(imageResizeRequest));
        }

        public HttpResult Get(MMImageResizeRequest imageResizeRequest)
        {
            try
            {
                var contentType = Helpers.GetContentTypeHttpResult(imageResizeRequest);
                var key = GetKeyImageResizeInRedis(imageResizeRequest);
                var imageResizeValue = SERedisManager.StringGetKey<string>(key);
                if (imageResizeValue != null)
                    return new HttpResult(Convert.FromBase64String(imageResizeValue), contentType);
                var byteImage = Helpers.GetByteArrayImage(imageResizeRequest);
                Save(imageResizeRequest, byteImage);
                return new HttpResult(byteImage, contentType);
            }
            catch (Exception exception)
            {
                throw new RedisProviderException(exception.Message)
                {
                    UrlImage = imageResizeRequest.PathSourceDecodeBase64
                };
            }
        }

        /// <summary>
        /// Gets the Redis key that is associated with the image.
        /// </summary>
        /// <param name="imageResizeRequest">The request dto</param>
        /// <returns>The key.</returns>
        public string GetKeyImageResizeInRedis(MMImageResizeRequest imageResizeRequest)
        {
            var json = JsonSerializer.SerializeToString(imageResizeRequest);
            var hashRequest = json.ToMd5();
            return "urn:imgresize:{0}:{1}:{2}".With( ConfigurationKeySettings.Stack.ApplicationId, imageResizeRequest.FileNameWithExtension,
                                 hashRequest);
        }

        private TimeSpan GetTimeout(MMImageResizeRequest resizeRequest)
        {
            var timeout = new TimeSpan(resizeRequest.Days, resizeRequest.Hours, resizeRequest.Minutes,
                resizeRequest.Seconds);
            if (timeout == new TimeSpan(0, 0, 0, 0))
                timeout = TimeoutKey;
            return timeout;
        }
    }
}