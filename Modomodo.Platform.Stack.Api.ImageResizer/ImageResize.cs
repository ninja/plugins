﻿using System;
using System.IO;
using System.Net;
using ImageResizer;
using Modomodo.Platform.Stack.Api.ImageResizer.Dto;

namespace Modomodo.Platform.Stack.Api.ImageResizer
{
    /// <summary>
    /// This class is responsible for building a resized image according to a given profile
    /// </summary>
    public class ImageResize
    {
        private ProfileResize Profile { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageResize"/> class.
        /// </summary>
        /// <param name="profileResize">The resizing profile</param>
        public ImageResize(ProfileResize profileResize)
        {
            Profile = profileResize;
        }

        /// <summary>
        /// Builds the resized image
        /// </summary>
        /// <param name="sourcePath">The Uri of the image to be resized</param>
        /// <returns>A byte array containing the resized image</returns>
        /// <exception cref="System.ArgumentNullException">sourcePath is null</exception>
        public byte[] BuildImage(Uri sourcePath)
        {
            if (sourcePath == null)
                throw new ArgumentNullException("sourcePath");

            using (var msDest = new MemoryStream())
            {
                var req = WebRequest.Create(sourcePath);
                using (var res = req.GetResponse())
                {
                    using (var smSource = res.GetResponseStream())
                    {
                        var image = new ImageJob(smSource, msDest, Profile.IRResizeSettings);
                        ImageBuilder.Current.Build(image);
                        return msDest.ToArray();
                    }
                }
            }
        }
    }
}