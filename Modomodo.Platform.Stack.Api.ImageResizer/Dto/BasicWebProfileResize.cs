﻿using System.Drawing;
using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Dto
{
    /// <summary>
    /// This defines base properties for resizing images and making them suitable for the Web  
    /// </summary>
    public class BasicWebProfileResize : IProfileResize
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BasicWebProfileResize"/> class.
        /// </summary>
        public BasicWebProfileResize()
        {
            Quality = 75;
            Format = OutputFormat.Png;
        }

        /// <summary>
        /// Gets or sets the background color of the image.
        /// </summary>
        /// <value>
        /// A Color object to specify the background color
        /// </value>
        public Color? BackgroundColor { get; set; }

        /// <summary>
        /// Gets or sets the output format of the image
        /// </summary>
        /// <value>
        /// An OutputFormat object to specify the output format of the resized image
        /// </value>
        public OutputFormat? Format { get; set; }

        /// <summary>
        /// Gets or sets the image height.
        /// </summary>
        public int? Height { get; set; }

        /// <summary>
        /// Gets or sets the maximum height of the image
        /// </summary>
        public int? MaxHeight { get; set; }

        /// <summary>
        /// Gets or sets the max width of the image
        /// </summary>
        public int? MaxWidth { get; set; }

        /// <summary>
        /// Gets or sets the image width.
        /// </summary>
        public int? Width { get; set; }

        /// <summary>
        /// Gets or sets the aspect ratio of the imange
        /// </summary>
        /// <value>
        /// An AspectRatio object to specity the right proportion
        /// </value>
        public AspectRatio? FitMode { get; set; }

        /// <summary>
        /// Gets or sets the quality of the imange
        /// </summary>
        /// <value>
        /// A percentage indicating the image quality (0-100)
        /// </value>
        public int? Quality { get; set; }

        /// <summary>
        /// Gets or sets degrees to rotate the image
        /// </summary>
        /// <value>
        /// Rotation degrees (e.g. 90)
        /// </value>
        public double? Rotate { get; set; }
    }
}
