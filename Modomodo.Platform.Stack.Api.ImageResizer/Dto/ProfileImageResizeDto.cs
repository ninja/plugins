﻿using System.Drawing;
using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Dto
{
    /// <summary>
    /// This class defines a DTO that can be used to define a basic image resize profile
    /// </summary>
    internal class ProfileImageResizeDto : IProfileResize
    {
        /// <summary>
        /// The image background color.
        /// </summary>
        public Color? BackgroundColor { get; set; }

        /// <summary>
        /// The output format.
        /// </summary>
        public OutputFormat? Format { get; set; }

        /// <summary>
        /// The image height.
        /// </summary>
        public int? Height { get; set; }

        /// <summary>
        /// The image max height.
        /// </summary>
        public int? MaxHeight { get; set; }

        /// <summary>
        /// The image max width.
        /// </summary>
        public int? MaxWidth { get; set; }

        /// <summary>
        /// The image width.
        /// </summary>
        public int? Width { get; set; }

        /// <summary>
        /// The aspect ratio that is used to mantain proportions.
        /// </summary>
        public AspectRatio? FitMode { get; set; }

        /// <summary>
        /// The image quality in percentage
        /// </summary>
        public int? Quality { get; set; }

        /// <summary>
        /// The rotation degrees
        /// </summary>
        public double? Rotate { get; set; }
    }
}
