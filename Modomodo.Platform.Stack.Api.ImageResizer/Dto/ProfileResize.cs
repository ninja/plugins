﻿using System;
using System.Linq;
using ImageResizer;
using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Dto
{
    /// <summary>
    /// This class manages a resize profile that can be used when using ImageResize functionalities
    /// </summary>
    public class ProfileResize
    {
        /// <summary>
        /// Resize settings for the current profile
        /// </summary>
        public ResizeSettings IRResizeSettings;

        private IProfileResize ProfileResizeSettings { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileResize"/> class.
        /// </summary>
        public ProfileResize()
        {
            IRResizeSettings = new ResizeSettings();
            ProfileResizeSettings = new BasicWebProfileResize();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileResize"/> class.
        /// </summary>
        /// <param name="profileResize">The resize profile to be used</param>
        public ProfileResize(IProfileResize profileResize)
            : this()
        {
            ProfileResizeSettings = profileResize;
            BuildCustomProfile();
        }

        /// <summary>
        /// Profile validation
        /// </summary>
        /// <exception cref="System.Exception">Each property of the associated IProfileResize is null</exception>
        public void ValidateProfile()
        {
            var allPropertiesNotNull = typeof(IProfileResize).GetProperties().Select(prop => prop.GetValue(ProfileResizeSettings, null))
                          .Where(val => val != null)
                          .ToList();

            if (allPropertiesNotNull.Count == 0)
                throw new Exception("ImageResize: The properties of profile can't be null");
        }

        /// <summary>
        /// Builds a custom profile according to the IProfileResize object that is injected in the constructor (by default is BasicWebProfileResize)
        /// </summary>
        public void BuildCustomProfile()
        {
            ValidateProfile();
            if (ProfileResizeSettings.BackgroundColor.HasValue)
                IRResizeSettings.BackgroundColor = ProfileResizeSettings.BackgroundColor.Value;

            if (ProfileResizeSettings.Format != null)
            {
                var format = ProfileResizeSettings.Format;
                IRResizeSettings.Format = format.Value.ToString().ToLower();
            }

            if (ProfileResizeSettings.Height.HasValue)
                IRResizeSettings.Height = ProfileResizeSettings.Height.Value;

            if (ProfileResizeSettings.MaxHeight.HasValue)
                IRResizeSettings.MaxHeight = ProfileResizeSettings.MaxHeight.Value;

            if (ProfileResizeSettings.MaxWidth.HasValue)
                IRResizeSettings.MaxWidth = ProfileResizeSettings.MaxWidth.Value;

            if (ProfileResizeSettings.Width.HasValue)
                IRResizeSettings.Width = ProfileResizeSettings.Width.Value;

            if (ProfileResizeSettings.FitMode != null)
                IRResizeSettings.Mode = (FitMode)ProfileResizeSettings.FitMode.Value;

            if (ProfileResizeSettings.Quality.HasValue)
                IRResizeSettings.Quality = SetMinOrMaxQuality(ProfileResizeSettings.Quality.Value);

            if (ProfileResizeSettings.Rotate.HasValue)
                IRResizeSettings.Rotate = ProfileResizeSettings.Rotate.Value;
        }

        private int SetMinOrMaxQuality(int quality)
        {
            if (quality < 0)
                return 0;
            if (quality > 100)
                return 100;
            return quality;
        }
    }
}
