﻿using System;
using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;
using Modomodo.Platform.Stack.Api.ImageResizer.Providers;
using ServiceStack.WebHost.Endpoints;

namespace Modomodo.Platform.Stack.Api.ImageResizer
{
    /// <summary>
    /// API for integration with Image Resize
    /// </summary>
    public class MMImageResizeFeature : IPlugin
    {
        public IProviderBase Provider { get; private set; }

        public MMImageResizeFeature()
        {
            Provider = new RedisProvider();
        }

        public MMImageResizeFeature(Type typeProvider, TimeSpan timeoutImageResize)
        {
            Provider = new RedisProvider();
            if (typeProvider != null && typeProvider == typeof (AmazonProvider))
                Provider = new AmazonProvider();
            else
                ((IProviderRedis) Provider).TimeoutKey = timeoutImageResize;
        }

        public void Register(IAppHost appHost)
        {
            appHost.RegisterService(typeof(MMImageResize), new[] { "/api/imageresize/path/{PathSourceBase64}" });
        }
    }
}