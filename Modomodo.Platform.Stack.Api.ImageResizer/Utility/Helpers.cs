﻿using System;
using Modomodo.Platform.Core.ExtensionMethods;
using Modomodo.Platform.Stack.Api.ImageResizer.Dto;
using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Utility
{
    /// <summary>
    /// This is a general-purpose Helper class 
    /// </summary>
    public class Helpers
    {
        /// <summary>
        /// Gets the image media type according to the given content type.
        /// </summary>
        /// <param name="request">The request dto.</param>
        /// <returns>A file extension.</returns>
        public static string GetContentTypeHttpResult(MMImageResizeRequest request)
        {
            if (request.Format == null)
                request.Format = OutputFormat.Png;
            return ".{0}".With( request.Format).MediaTypesExtension();
        }

        /// <summary>
        /// Convert a file object into an array of bytes
        /// </summary>
        /// <param name="request">The request dto</param>
        /// <returns>Byte array</returns>
        public static byte[] GetByteArrayImage(MMImageResizeRequest request)
        {
            var profileImageResizeDto = new ProfileImageResizeDto
            {
                BackgroundColor = request.BgColor,
                FitMode = request.FitMode,
                Format = request.Format,
                Height = request.H,
                MaxHeight = request.MaxH,
                MaxWidth = request.MaxW,
                Quality = request.Quality,
                Rotate = request.Rotate,
                Width = request.W
            };

            var imageResize = new ImageResize(new ProfileResize(profileImageResizeDto));
            var byteArrayImage = imageResize.BuildImage(new Uri(request.PathSourceDecodeBase64));
            return byteArrayImage;
        }
    }
}
