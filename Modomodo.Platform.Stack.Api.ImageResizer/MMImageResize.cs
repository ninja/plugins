﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using Modomodo.Platform.Core.ExtensionMethods;
using Modomodo.Platform.SERedis.Interfaces;
using Modomodo.Platform.Stack.Api.ImageResizer.Exceptions;
using Modomodo.Platform.Stack.Api.ImageResizer.Interfaces;
using Modomodo.Platform.Stack.Api.ImageResizer.Providers;
using Modomodo.Platform.Stack.Common;
using Modomodo.Platform.Stack.Interfaces;
using ServiceStack.Common.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace Modomodo.Platform.Stack.Api.ImageResizer
{
    /// <summary>
    /// This service permits to manage images for resizing.
    /// </summary>
    public class MMImageResize : Service
    {
        public ISERedisManager SeRedisManager { get; set; }

        /// <summary>
        /// There was an error when resizing an image.
        /// </summary>
        public const string GenericMessageImageResize = "Modomodo.Stack: Error during image resize generation";

        public const string ErrorGetImage = "Modomodo.Stack: error get image with url {0}";

        /// <summary>
        /// Accept the GET verb for the specified request dto.
        /// </summary>
        /// <param name="request">The request dto</param>
        /// <returns>The http result of the operation</returns>
        /// <exception cref="Exception">An internal server error occurred.</exception>
        public HttpResult Get(MMImageResizeRequest request)
        {
            try
            {
                IProviderBase provider = new RedisProvider();
                var plugins = GetAppHost().Plugins;
                if (plugins == null || plugins.Count == 0)
                    return provider.Get(request);

                var pluginImageResizer = plugins.FirstOrDefault(x => x.GetType() == typeof (MMImageResizeFeature));
                if (pluginImageResizer != null)
                    provider = ((MMImageResizeFeature) pluginImageResizer).Provider;
                return provider.Get(request);
            }
            catch (RedisProviderException exception)
            {
                throw new StackHttpError(HttpStatusCode.InternalServerError, exception.Message, string.Format(ErrorGetImage, exception.UrlImage));
            }
            catch (Exception exception)
            {
                throw new StackHttpError(HttpStatusCode.InternalServerError, exception.Message, GenericMessageImageResize);
            }
        }
    }

    /// <summary>
    /// The request dto to provide information about the image to be resized.
    /// </summary>
    [Route("/api/imageresize/path/{PathSourceBase64}", Verbs = Verbs.Get)]
    public class MMImageResizeRequest : IReturn<HttpResult>
    {
        /// <summary>
        /// The base64 source path is empty
        /// </summary>
        public const string MessageImageResizeNotValidPath = "Modomodo.Stack: Path Source base 64";

        /// <summary>
        /// Gets or sets the base64 encoded source path.
        /// </summary>
        public string PathSourceBase64 { get; set; }

        [AvoidAttribute]
        internal string PathSourceDecodeBase64
        {
            get
            {
                if (string.IsNullOrWhiteSpace(PathSourceBase64))
                    throw new ArgumentNullException(MessageImageResizeNotValidPath);
                return PathSourceBase64.FromBase64();
            }
        }

        [AvoidAttribute]
        internal string FileNameWithExtension
        {
            get
            {
                return Path.GetFileName(PathSourceDecodeBase64);
            }
        }

        [AvoidAttribute]
        internal string FileExtension
        {
            get { return Path.GetExtension(PathSourceDecodeBase64); }
        }

        /// <summary>
        /// Background Color
        /// </summary>
        public Color? BgColor { get; set; }

        /// <summary>
        /// The output image format (Jpg, Png and Gif)
        /// </summary>
        public OutputFormat? Format { get; set; }

        /// <summary>
        /// Image Height
        /// </summary>
        public int? H { get; set; }

        /// <summary>
        /// Image max height
        /// </summary>
        public int? MaxH { get; set; }

        /// <summary>
        /// Image max width
        /// </summary>
        public int? MaxW { get; set; }

        /// <summary>
        /// Possible aspect ratio proportions such as None, Max, Pad, Crop, Carve, Stretch (Default: Pad)
        /// </summary>
        public AspectRatio? FitMode { get; set; }

        /// <summary>
        /// The quality of the ouput format (for Jpg)
        /// </summary>
        public int? Quality { get; set; }

        /// <summary>
        /// The rotation angle.
        /// </summary>
        public double? Rotate { get; set; }

        /// <summary>
        /// Image Width
        /// </summary>
        public int? W { get; set; }

        /// <summary>
        /// Days timeout
        /// </summary>
        public int Days { get; set; }

        /// <summary>
        /// Hours timeout
        /// </summary>
        public int Hours { get; set; }

        /// <summary>
        /// Minutes timeout
        /// </summary>
        public int Minutes { get; set; }

        /// <summary>
        /// Seconds timeout
        /// </summary>
        public int Seconds { get; set; }
    }
}