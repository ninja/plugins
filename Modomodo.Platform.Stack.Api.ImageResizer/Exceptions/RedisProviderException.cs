﻿using System;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Exceptions
{
    public class RedisProviderException : Exception
    {
        public RedisProviderException(string message) : base(message)
        {   
        }

        public string UrlImage { get; set; }
    }
}
