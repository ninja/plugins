﻿namespace Modomodo.Platform.Stack.Api.ImageResizer.Interfaces
{
    /// <summary>
    /// Possible output format 
    /// </summary>
    public enum OutputFormat
    {
        /// <summary>
        /// JPG format
        /// </summary>
        Jpg,

        /// <summary>
        /// PNG format
        /// </summary>
        Png,

        /// <summary>
        /// GIF format
        /// </summary>
        Gif
    }
}