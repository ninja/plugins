﻿using ServiceStack.Common.Web;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Interfaces
{
    /// <summary>
    /// Provides a base implementation for the extensible provider model
    /// </summary>
    public interface IProviderBase
    {
        void Save(MMImageResizeRequest imageResizeRequest, byte[] imageResize);
        HttpResult Get(MMImageResizeRequest request);
    }
}