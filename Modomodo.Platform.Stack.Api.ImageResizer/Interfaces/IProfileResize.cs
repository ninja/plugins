﻿using System.Drawing;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Interfaces
{
    /// <summary>
    /// This interface defines properties for resizing images
    /// </summary>
    public interface IProfileResize
    {
        //TODO: non wrappati cache, border, border, crop manual, padding, paddingColor, processWhen, scale, sourceFlip, stretch

        /// <summary>
        /// Background color
        /// </summary>
        Color? BackgroundColor { get; set; }

        /// <summary>
        /// Output format
        /// </summary>
        OutputFormat? Format { get; set; }

        /// <summary>
        /// Image height.
        /// </summary>
        int? Height { get; set; }

        /// <summary>
        /// Image max height
        /// </summary>
        int? MaxHeight { get; set; }

        /// <summary>
        /// Image max width
        /// </summary>
        /// <value>
        /// The width of the max.
        /// </value>
        int? MaxWidth { get; set; }

        /// <summary>
        /// Image width
        /// </summary>
        int? Width { get; set; }

        /// <summary>
        /// Image aspect ratio for proportions
        /// </summary>
        AspectRatio? FitMode { get; set; }

        /// <summary>
        /// Image quality in percentage
        /// </summary>
        int? Quality { get; set; }

        /// <summary>
        /// Rotation degrees
        /// </summary>
        double? Rotate { get; set; }
    }
}