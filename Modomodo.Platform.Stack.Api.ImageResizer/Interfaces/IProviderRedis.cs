﻿using System;

namespace Modomodo.Platform.Stack.Api.ImageResizer.Interfaces
{
    /// <summary>
    /// Redis provider base
    /// </summary>
    public interface IProviderRedis : IProviderBase
    {
        TimeSpan TimeoutKey { get; set; }
    }
}